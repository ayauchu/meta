# Ayanami personnel processing and records

## How to register

Create a merge request to add your information to the list of personnel in the file ```personnel.csv```. You must provide your preferred name, email address, and GitLab username.
