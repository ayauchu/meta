# Ayanami meta-information

This repository contains various information concerning the organization Ayanami Space itself, rather than any of its projects.

## Principles

- Document all relevant and useful information, no matter how small.
- Make information accessible to everyone and communicate it with accuracy and precision.
